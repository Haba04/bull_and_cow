const randomArray = () => {
    const number = []

    while (number.length < 4) {
        const newNumber = Math.round(Math.random() * 10)
        if (newNumber < 10 && !number.includes(newNumber)) number.push(newNumber)
    }
    return number
}

const ranArr = randomArray()

console.log(ranArr)

const addNum = num => {
    const userNumber = document.getElementById('user').value
    const userArray = []
    for (let i = 0; i < 4; i++) {
        userArray.push(parseInt(userNumber.substr(i, 1)))
    }
    console.log(userArray)

    compareArray(userArray)
}

const compareArray = userArray => {
    let bull = 0
    let cow = 0
    let status = 'Вы проиграли'

    for (let i = 0; i < 4; i++) {
        if (userArray[i] === ranArr[i]) bull++
        else if (ranArr.includes(userArray[i])) cow++
    }

    console.log(`Bulls: ${bull}, cows: ${cow}`)

    if (bull === 4) {
        status = 'Вы выиграли'
        succes(userArray, bull, cow, status)
    } else {
        result(userArray, bull, cow)
    }
}

const succes = (userArray, bull, cow, status) => {
    document.getElementById('result').innerHTML = `Ваш ответ: ${userArray}, быков: ${bull}, коров: ${cow} <br/> <h2>${status}</h2>`
}

const result = (userArray, bull, cow) => {
    document.getElementById('result').innerHTML = `Ваш ответ: ${userArray}, быков: ${bull}, коров: ${cow}`
}